<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba</title>
    <link rel="stylesheet" href="{{ URL::asset('storage/css/miscss/estilos.css') }}">
</head>
<body>
    <p>Esto es un párrafo de prueba.</p>
    <br/>
    <img src="{{ URL::asset('storage/fotos/phantom.png') }}">
    <script src="{{ URL::asset('storage/js/misjs/main.js') }}"></script>
</body>
</html>